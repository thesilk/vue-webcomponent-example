import { shallowMount } from "@vue/test-utils";
import WebComponent from "@/components/WebComponent.vue";

Object.assign(navigator, {
  clipboard: {
    writeText: () => {
      return;
    },
  },
});

describe("WebComponent.vue", () => {
  const user = "tester";
  const wrapper = shallowMount(WebComponent, {
    propsData: { user },
  });
  it("renders props.user when passed", () => {
    expect(wrapper.text()).toMatch(`Hello tester!`);
  });

  it("should copy to clipboard", async () => {
    jest.spyOn(navigator.clipboard, "writeText");

    const btn = wrapper.find("#copy-to-clipboard-btn");
    btn.trigger("click");
    expect(navigator.clipboard.writeText).toHaveBeenCalledWith(
      "https://duckduckgo.com/tester"
    );
  });
});
