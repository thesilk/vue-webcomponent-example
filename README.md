# web-component-demo-2

## How to modify the custom component with a shadow dom?

`document.querySelector('custom-element').shadowRoot.querySelector('#copy-to-clipboard-btn').style.border = "1px solid white"`

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your unit tests
```
npm run test:unit
```

### Run your end-to-end tests
```
npm run test:e2e
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
